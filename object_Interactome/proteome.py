
path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/protein_uniprotID_name.txt'

class Proteome :
    """Associates protein names with their UniprotID."""
    
    protein_list = []
    dict_ID_name = {}
    dict_name_ID = {}
    
    
    def __init__ (self, file) :
        self.proteins = self.protein_list(file)
        self.dict_name_ID = self.make_dict_name_ID(file)
        self.dict_ID_name = self.make_dict_ID_name(file)

    
    def protein_list(self, path) :
        """
        Gives the list of proteins from the file contaning the names + UniProt IDs.
        
        Parameters
        ----------
        path : str
            the path to the file of the interaction graph
        
        Returns
        -------
        list
            proteins in the graph
        """
        
        list_prot = []
        with open(path) as file_prot :
            for line in file_prot :
                if '\t' in line :
                    name_ID = line.split('\t')
                    prot_name = name_ID[0]
                    list_prot.append(prot_name)
        return(list_prot)
    
    def make_dict_ID_name(self, path) :
        """
        Creates a dictionnary that gives the name of the protein when searching by ID.
        
        Parameters
        ----------
        path : str
            the path to the file of the interaction graph
        
        Returns
        -------
        dict
            gives the UniprotID corresponding to a protein name
        """
        
        dict_ID_name = {}
        with open(path) as file_prot :
            for line in file_prot :
                if '\t' in line :
                    name_ID = line.split('\t')
                    prot_name = name_ID[0]
                    prot_ID = name_ID[1][:-1]
                    if prot_ID not in dict_ID_name.keys() :
                        dict_ID_name[prot_ID] = []
                    dict_ID_name[prot_ID].append(prot_name)
        return(dict_ID_name)
    
    def make_dict_name_ID(self, path) :
        """
        Creates a dictionnary that gives the name of the protein when searching by ID.
        
        Parameters
        ----------
        path : str
            the path to the file of the interaction graph
        
        Returns
        -------
        dict
            gives the name of a protein corresponding to its UniprotID
        """
        
        dict_name_ID = {}
        with open(path) as file_prot :
            for line in file_prot :
                if '\t' in line :
                    name_ID = line.split('\t')
                    prot_name = name_ID[0]
                    prot_ID = name_ID[1][:-1]
                    if prot_name not in dict_name_ID.keys() :
                        dict_name_ID[prot_name] = []
                    dict_name_ID[prot_name].append(prot_ID)
        return(dict_name_ID)
    


