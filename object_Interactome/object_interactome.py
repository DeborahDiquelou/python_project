########## Chapter 3 ##########

import math
import proteome as pr
import domainGraph as dg
import urllib3
import re
import pickle
import matplotlib.pyplot as plt
import os

path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/Human_HighQuality.txt'
path2 = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/protein_uniprotID_name.txt'

new_proteome = pr.Proteome(path)

class Interactome :
    """The class Interactome allows to read a graph of interactions ans store it in an object Interactome."""
    
    int_dict = {}
    int_list = []
    proteins = []
    vetices_num = []
    edges = []
    cc_dict = {}
    cc_size = {}
    cc_count = []
    dict_graph = {}
    domains = []
    
    def __init__ (self, file) :
        self.int_dict = self.read_interaction_file_dict(file)
        self.int_list = self.read_interaction_file_list(file)
        self.proteins = self.vertices_list()
        self.vertices_num = self.count_vertices
        self.edges = self.count_edges
        self.cc_dict = self.countCC()[0]
        self.cc_size = self.countCC()[1]
        self.cc_count = self.countCC()[2]
        self.dict_graph = self.xlink_Domains()
        self.domains = self.ls_domains()
        
        
    ### Methods for attributes
        
    def read_interaction_file_dict(self, file) :
        """
        Reads an interactions' graph of proteins and convert it into a dictionnary of interactions.
        
        Parameters
        ----------
        file
            path to interaction graph
        
        Returns
        -------
        dict
            interactions of the graph graph with every protein as key, the values are their connected proteins
        """
        
        file_test = open(file, 'r')
        network = {}
        for i in file_test :
            if '\t' in i : # if i is not the line indicating the number of interactions
                prot = i.split('\t')
                prot_A = prot[0]
                prot_B = prot[1][:-1] # delete the newline character
                if prot_A not in network.keys() :
                    network[prot_A] = []
                if prot_B not in network.keys() :
                    network[prot_B] = []
                if prot_B not in network[prot_A] :
                    network[prot_A].append(prot_B)
                if prot_A not in network[prot_B] :
                    network[prot_B].append(prot_A)
        file_test.close()
        return(network)
    
    def read_interaction_file_list(self, file) :
        """
        Reads an interactions' graph of proteins and convert it into a list of couples of interactions.
        
        Parameters
        ----------
        file
            path to interaction graph
        
        Returns
        -------
        list
            interactions of the graph represented as tuples of connected proteins
        """
        
        file_test = open(file, 'r')
        list_couples = []
        for i in file_test :
            if '\t' in i : # if i is not the line indicating the number of interactions
                prot = i.split('\t')
                prot_A = prot[0]
                prot_B = prot[1][:-1] # delete the newline character
                proteins = (prot_A, prot_B)
                list_couples.append(proteins)
        file_test.close()
        return(list_couples)
    
    def vertices_list(self) :
        """
        Gives the list of proteins present in the graph.
        
        Returns
        -------
        list
            the proteins of the graph
        """
        
        vertices_list = []
        for key in self.int_dict.keys() :
            if key not in vertices_list :
                vertices_list.append(key)
        return(vertices_list)
    
    ### Chapter 2 methods
    
    def count_vertices(self) :
        """
        Gives the number of vertices in the interactions' graph from the number of keys in the dictionnary of interactions.
        
        Returns
        -------
        int
            the number of vertices
        """
        
        number_keys = len(self.int_dict)
        return(number_keys)
    
    def count_edges(self) :
        """
        Gives the number of edges in the interactions' graph from the list of interactions.
        
        Returns
        -------
        int
            the number of edges
        """
        
        number_edges = len(self.int_list)
        return number_edges
    
    def clean_interactome(self) :
        """
        From the interaction file stored in the class Interactome, creates another one without interactions present
        in several copies in the first file, or without auto-interactions.
        """
        
        list_interactions = []
        for uplet in self.int_list :
            prot_A = uplet[0]
            prot_B = uplet[1]
            uplet_2 = (prot_B, prot_A)
            if uplet not in list_interactions :
                if uplet_2 not in list_interactions :
                    if prot_A != prot_B :
                        list_interactions.append(uplet)
        number_interactions = str(len(list_interactions))
        graph_cleaned = open('graph_cleaned', 'w')
        graph_cleaned.write(str(number_interactions + '\n'))
        for uplet in list_interactions :
            prot_A = uplet[0]
            prot_B = uplet[1]
            new_interaction = prot_A, prot_B
            new_interaction_bis = '\t'.join(new_interaction)
            graph_cleaned.write(str(new_interaction_bis + '\n'))
        graph_cleaned.close()
    
    ### Chapter 2: Degree
        
    def get_degree(self, prot) :
        """
        Takes a protein as argument to count the number of its interactions in the graph.
        
        Parameters
        ----------
        prot : str
            the name of a protein oof the graph
        
        Returns
        -------
        int
            the number of the protein's interactions
        """
        
        return(len(self.int_dict[prot]))
        
    def get_max_degree(self) :
        """
        Finds the protein having the highest number of interactions in the graph.
        
        Returns
        -------
        str
            the protein having the highest number of interactions
        """
        
        degree_list = []
        for protein in self.proteins :
            degree = self.get_degree(protein)
            degree_uplet = (protein, degree)
            degree_list.append(degree_uplet)
        degree_max = 0
        for uplet in degree_list :
            protein = uplet[0]
            degree = uplet[1]
            if degree > degree_max :
                degree_max = degree
                protein_max = protein
            elif degree == degree_max :
                protein_max = (protein_max, protein)
        return(protein_max)
    
    def get_ave_degree(self) :
        """
        Computes the average number of interactions in the graph.
        
        Returns
        -------
        int
            the average number of interactions
        """
        
        degrees = []
        for protein in self.proteins :
            degrees.append(self.get_degree(protein))
        vertices_nb = self.count_vertices()
        degree_sum = sum(degrees)
        ave_degree = degree_sum / vertices_nb
        return(ave_degree)
    
    def count_degree(self, deg) :
        """
        Takes a number of interactions as argument to return the number of proteins having this specific degree of interactions.
        
        Parameters
        ----------
        deg : int
            the number of interactions the proteins must have to be counted
        
        Returns
        -------
        int
            the number of proteins having a precise amount of interactions
        """
        
        degree_list = []
        for protein in self.proteins :
            degree = self.get_degree(protein)
            prot_degree = (protein, degree)
            degree_list.append(prot_degree)
        count = 0
        for uplet in degree_list :
            protein = uplet[0]
            degree = uplet[1]
            if degree == deg :
                count += 1
        return(count)
    
    def histogram_degree(self, dmin, dmax) :
        """
        Finds, for a range of interactions' number, the number of proteins having a specific degree.
        The result is displayed by a number indicating the degree followed by a number of asterisks representing
        the number of proteins having this degree of interactions.
        
        Parameters
        ----------
        dmin : int
            the minimum number of interactions the proteins must have to be counted
        dmax : int
            the maximum number of interactions the proteins must have to be counted
        """
        
        proteins_degrees = []
        dmax = dmax + 1
        for d in range(dmin, dmax) :
            count = self.count_degree(d)
            degree_count = (d, count)
            proteins_degrees.append(degree_count)
        for uplet in proteins_degrees :
            number_prot_degree = ''
            deg = str(uplet[0])
            count = uplet[1]
            
            # To have a scale in the representation of interactions
            count_truncate = math.trunc(count/10)
            
            for number in range(count_truncate) :
                number_prot_degree += '*'
            print(deg + number_prot_degree)
    

    ### CHAPTER 4 --- Connected Components ###
        
    def findCC(self, flag, protein, count, cc) :
        """
        Iterates through a graph to find the connected component in which a protein belongs.
        To do this, it finds neighbours of the protein in argument and neighbours of its neighbours (recursive function and depth search).
        It results in a dictionnary storing the connected components.
        
        Parameters
        ----------
        flag : dict
            the dictionnary of proteins associated to the value -1
        protein : str
            the protein for which we search the connected components
        count : int
            the size of the connected component
        cc : dict
            the connected component
        
        Returns
        -------
        dict
            the complete connected component
        """
        
        flag[protein] = 1
        if count not in cc.keys() :
            cc[count] = []
        cc[count].append(protein)
        for adjacent_node in self.int_dict[protein] :
            if flag[adjacent_node] == -1 :
                self.findCC(flag, adjacent_node, count, cc)
        return(cc)
    
    def countCC(self) :
        """
        Finds connected components in a graph by calling the function findCC for each protein that has not been passed through the function yet.
        It returns a dictionnary of all connected components in the graph, their size and number.
        
        Returns
        -------
        dict
            the connected components in the graph
        dict
            the size of each connected component
        int
            the number of connnected components in the graph
        """
        
        flag = {}
        cc = {}
        cc_size = {}
        for protein in self.proteins :
            if protein not in flag.keys() :
                flag[protein] = -1
        count = 0
        for protein in self.proteins :
            if flag[protein] == -1 :
                count += 1
                self.findCC(flag, protein, count, cc)
        for key in cc.keys() :
            cc_size[key] = len(cc[key])
        return(cc, cc_size, count)
        
    def writeCC(self) :
        """
        Creates a text file in the working directory to store the connected components and their size.
        """
        
        cc_file = open('./cc_file.txt', 'w')
        for key in self.cc_dict.keys() :
            size = self.cc_size[key]
            cc_prot = self.cc_dict[key]
            line = str(size), str(cc_prot)
            line_to_add = '\t'.join(line)
            cc_file.write(line_to_add)
            cc_file.write('\n')
        cc_file.close()
        
    def extractCC(self, prot) :
        """
        Finds the connected component in which a protein belongs to.
        
        Parameters
        ----------
        prot : str
            the protein for which we want to know its connected component
        
        Returns
        -------
        dict
            the connected component of the protein
        """
        
        for cc in self.cc_dict.keys() :
            if prot in self.cc_dict[cc] :
                return(self.cc_dict[cc])
            
    def computeCC(self, num) :
        """
        Finds a specific protein in a list and its connected component.
        
        Parameters
        ----------
        num : int
            the position of the protein in the list
        
        Returns
        -------
        str
            the name of the protein at the position 'num'
        """
        
        prot = self.proteins[num]
        for key in self.cc_dict.keys() :
            if prot in self.cc_dict[key] :
                return(key)
    
        ### Density and clustering coefficient ###
    
    def density(self) :
        """
        Computes the density of the graph.
        
        Returns
        -------
        int
            the density of the graph
        """
        
        density_numerator = 2*int(self.edges())
        density_denominator = int(self.vertices_num())*(int(self.vertices_num())-1)
        density = density_numerator/density_denominator
        return(density)

    def clustering(self, prot) :
        """
        Calculates the clustering coefficient of the protein in argument.
        The function first finds its neighbours, and then every triangles that could be made between the protein and two of her neighbours.
        Finally the coefficient is calculated by dividing the number of possible triangles by the number of neighbours.
        
        Parameters
        ----------
        prot : str
            the protein studied
        
        Returns
        -------
        float
            the clustering coefficient of the protein
        """
        
        # Find pairs of neighbours of prot
        interactions_of_prot = self.int_dict[prot]
        pairs_neighbours = []
        if self.get_degree(prot) <= 1 :
            cluster_coef = 0
        else :
                
            for protein_1 in interactions_of_prot :
                for protein_2 in interactions_of_prot :
                    if protein_1 != protein_2 :
                        pair = (protein_1, protein_2)
                        pairs_neighbours.append(pair)
            # Find triangles of prot
            count_triangle = 0
            for pair in pairs_neighbours :
                prot_1 = pair[0]
                prot_2 = pair[1]
                if prot_2 in self.int_dict[prot_1] :
                    count_triangle+=1
            # Compute the clustering coefficient of prot
            cluster_coef = count_triangle/len(pairs_neighbours)
        return(cluster_coef)


    ### CHAPTER 5 --- Protein Domains ###
    
# 5.2.1
# The Pfam database is a large collection of protein domain families.
# The Pfam uses the UniProt database to give a summary of the protein, and retrieve its sequence to display protein domains.
# For each protein domain there is an informativ page about them.

#5.2.2
# The UniProt ID of INSR_HUMAN is P06213.
    
    def create_file_list_protein(self) :
        """
        In order to have UniProt IDs of the proteins in the graph file, the function creates a new file with
        each names to upload separately on https://www.uniprot.org/uploadlists/.
        (For question 5.3.1).
        """
        
        prot_names_list = open("protein_names_list.txt", "w")
        for protein in self.proteins :
            prot_names_list.write(protein + '\n')          

    def xlink_Uniprot(self) :
        """
        Gives for each protein of the interactome its UniProt ID and its neighbours thanks to the class Proteome.
        The newly created dictionnary is an improve version of the dictionnary storing the graph in the class Interactome,
        as it adds two sub-dictionnaries, one for the Uniprot ID and another for the neighbours.
        
        Returns
        -------
        dict
            interaction graph stored with vertices as keys, for which two types of values are possible: the Uniprot ID and the neighbours
        """
        
        dict_graph = {}
        for key in self.int_dict.keys() :
            if key not in dict_graph.keys() :
                dict_graph[key] = {}
                dict_graph[key]['UniprotID'] = []
                dict_graph[key]['voisins'] = self.int_dict[key]
            for key2 in new_proteome.dict_name_ID.keys() :
                if key == key2 :
                    dict_graph[key]['UniprotID'] = new_proteome.dict_name_ID[key2]
        return(dict_graph)
    
# 5.4.1
# https://www.uniprot.org/uniprot/P06213.txt
# Protein domains and number:
# DR   Pfam; PF00757; Furin-like; 1.
# DR   Pfam; PF17870; Insulin_TMD; 1.
# DR   Pfam; PF07714; PK_Tyr_Ser-Thr; 1.
# DR   Pfam; PF01030; Recep_L_domain; 2.
                
    def get_protein_domains(self, p) :
        """
        Searches for the Uniprot page on the Internet about a protein, to extract its protein domains as described by the Pfam database.
        
        Parameters
        ----------
        p : str
            the protein studied
        
        Returns
        -------
        list
            the protein domains belonging to the studied protein
        """
        
        # Get the url corresponding to protein p
        p_ID = str(self.dict_graph[p]['UniprotID'])[2:-2]
        url_end = (p_ID, 'txt')
        url_end_join = '.'.join(url_end)
        url = 'https://www.uniprot.org/uniprot/' + url_end_join
        
        # Get the informations inside the webpage
        http = urllib3.PoolManager()
        response = http.request('GET', url)
        webpage = response.data
        webpage_str = webpage.decode("utf-8").split("\n")
        
        # Extract protein domains
        domains = []
        for line in (webpage_str):
            if 'Pfam' in line :
                line2 = line.split(" ")
                domain_nb = (line2[5][:-1])
                domains.append(domain_nb)
        return(domains)
    
    def xlink_Domains(self) :
        """
        Improves the interaction graph dictionnary by adding a sub-dictionnary 'Domains' to describe the proteins of the interactome.
        The protein domains of the protein are found and stored in this sub-dictionnary thanks to the function get_protein_domains().
        
        Returns
        -------
        dict
            an improved interaction graph, compared to the dictionnary returned by the function xlink_Uniprot()
        """
        
        if (os.path.isfile("dict_graph.pkl")) :
            with open("dict_graph.pkl", "rb") as pklFile :
                self.dict_graph = pickle.load(pklFile)
                return(self.dict_graph)
        else :
            self.dict_graph = self.xlink_Uniprot()
            for key in self.dict_graph.keys() :
                self.dict_graph[key]['Domains'] = []
            for protein in self.proteins :
                domains = self.get_protein_domains(protein)
                for key in self.dict_graph.keys() :
                    if protein == key :
                        for dom in domains :
                            if dom not in self.dict_graph[protein]['Domains'] :
                                self.dict_graph[protein]['Domains'].append(dom)
            with open("dict_graph.pkl", "wb") as pklFile:
                pickle.dump(self.dict_graph, pklFile)
            return(self.dict_graph)
    
    
    ### CHAPTER 6 --- Protein Domains Analysis ###

    def fonctionls_proteins(self) :
        """
        Gives the list of proteins in the interaction graph.
        
        Returns
        -------
        list
            the proteins of the graph
        """
        
        return(self.proteins)
    
    
    def ls_domains(self) :
        """
        Returns the list of domains present in the interaction graph.
        
        Returns
        -------
        list
            protein domains of the graph
        """
        
        ls_domains = []
        for prot in self.proteins :
            for dom in self.dict_graph[prot]['Domains'] :
                if dom not in ls_domains :
                    ls_domains.append(dom)
        return(ls_domains)
    
    
    def ls_domains_n(self, n) :
        """
        Iterates through the dictionnary of proteins and domains to compute how many times domains are registered in the dictionnary.
        If they are present at least n times, the function returns them in a list.
        
        Parameters
        ----------
        n : float
            the number of times a domain must appear in the graph to be counted
            
        Returns
        -------
        list
            number of domains being n times in the graph
        """
        
        iteration = {}
        for prot in self.proteins :
            for dom in self.dict_graph[prot]['Domains'] :
                if dom not in iteration.keys() :
                    iteration[dom] = 1
                else :
                    iteration[dom] += 1
        domains_n = []
        for dom in iteration.keys() :
            if iteration[dom] <= n :
                domains_n.append(dom)
        return(domains_n)
    
    def nbDomainsByProteinDistribution(self, value) :
        """
        Creates a dictionnary with the possible numbers of domains found in one protein as keys, and to each key corresponds
        the number of proteins having this number of protein domains.
        Displays a histogramm showing this distribution.
        
        Parameters
        ----------
        value : int
            a flag to display (= 1) or not (!= 1) the histogram
        
        Returns
        -------
        dict
            the number of domains that can be found in one protein
        """
        
        dom_distri = {}
        x_axis = []
        
        n_dom = []
        for prot in self.proteins:
            nb = len(self.dict_graph[prot]["Domains"])
            if nb not in dom_distri.keys() :
                dom_distri[nb] = 0
            dom_distri[nb]+=1
            x_axis.append(nb)

        if value == 1 :
            plt.hist(x_axis, bins=10)
            plt.show()
        return(dom_distri)

    def nbProteinsByDomainDistribution(self, value) :
        """
        Creates a dictionnary with the numbers of domains that one protein can have, and their frequence in the proteins.
        Displays a histogramm showing this distribution.
        
        Parameters
        ----------
        value : int
            a flag to display (= 1) or not (!= 1) the histogram
        
        Returns
        -------
        dict
            the number of proteins having the possible number of domains
        """
        dom_distri = self.nbDomainsByProteinDistribution(0)
        dom_distri_2 = {}
        x_axis = []
        for key, value in dom_distri.items():
            dom_distri_2.setdefault(value, list()).append(key)
        for key in dom_distri_2.keys() :
            x_axis.append(key)
            
        if value == 1 :
            plt.hist(x_axis, bins=10)
            plt.show()
        return(dom_distri_2)
    
    def co_occurrence(self, dom_x, dom_y) :
        """
        Counts the number of times that two given domains are found in the same protein.
        
        Parameters
        ----------
        dom_x : str
            a protein domain
        dom_y : str
            a protein domain
        
        Returns
        -------
        int
            the number of times dom_x and dom_y are co-occurrent in one protein
        """
        co_occ = 0
        for prot in self.proteins :
            if dom_x in self.dict_graph[prot]["Domains"] and dom_y in self.dict_graph[prot]["Domains"] :
                co_occ += 1
        return(co_occ)

    def weighted_cooccurrence_graph(self, path) :
        """
        Creates an object Domain graph and returns a weighted graph of interacted protein domains included in the Interactome.
        These domains must be co-occurrent at least one time in Interactome to be vertices of the weighted graph.
        
        Parameters
        ----------
        path : str
            path to the interaction graph file
        
        Returns
        -------
        dict
            the dictionnary of the graph from a new object of class DomainGraph
        """
        
        dgraph = dg.DomainGraph(path)
        dom_graph = dgraph.generate_cooccurrence_weighted_graph(path)
        return(dom_graph.dict_graph_dom)

    def weighted_cooccurrence_graph_n(self, path, n) :
        """
        Creates an object Domain graph and returns a weighted graph of interacted protein domains included in the Interactome.
        These domains must be co-occurrent at least one time in Interactome to be vertices of the weighted graph.
        
        Parameters
        ----------
        path : str
            path to the interaction file
        n : int
            minimal number of co-occurrencies wanted between domains
        
        Returns
        -------
        dict
            the dictionnary of the graph from a new object of class DomainGraph
        """
        dgraph = dg.DomainGraph(path)
        dom_graph = dgraph.generate_cooccurrence_weighted_graph_n(path, n)
        return(dom_graph.dict_graph_dom)
    
    def export_dom_graph(self, path) :
        """
        Creates a file as a gdf to export the graph and visualize it under Gephi.
        
        Parameters
        ----------
        path : str
            path to the interaction file
        """
        
        graph = self.weighted_cooccurrence_graph(path)
        outfile_gdf = open("networkDOM.gdf","w")
        # GDF format
        outfile_gdf.write("nodedef>name VARCHAR\n")
        for key in graph.keys() :
            if len(key) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                outfile_gdf.write(str(key)+"\n")
        outfile_gdf.write("edgedef>node1 VARCHAR, node2 VARCHAR, weight DOUBLE\n")
        for key in graph.keys() :
            if len(key) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                node1 = key
                for dom in graph[key] :
                    node2 = dom[0]
                    if len(node2) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                        weight = str(dom[1])
                        to_write = node1, node2, weight
                        outfile_gdf.write(','.join(to_write)+"\n")
        outfile_gdf.close()

    def graph_threshold(self, path, k) :
        """
        Creates a file as a gdf to export the graph and visualize it under Gephi.
        The graph stored in the gdf file must have edges with a minimum weight of k.
        
        Parameters
        ----------
        path : str
            path to the interaction file
        k : int
            minimum weight of edges wanted
        """
        
        graph = self.weighted_cooccurrence_graph_n(path, k)
        outfile_gdf = open("networkDOM_threshold.gdf","w")
        # GDF format
        outfile_gdf.write("nodedef>name VARCHAR\n")
        for key in graph.keys() :
            if len(key) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                outfile_gdf.write(str(key)+"\n")
        outfile_gdf.write("edgedef>node1 VARCHAR, node2 VARCHAR, weight DOUBLE\n")
        for key in graph.keys() :
            if len(key) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                node1 = key
                for dom in graph[key] :
                    node2 = dom[0]
                    if len(node2) > 0 : # avoid domains with no name -> come from proteins with no Uniprot ID
                        weight = str(dom[1])
                        to_write = node1, node2, weight
                        outfile_gdf.write(','.join(to_write)+"\n")
        outfile_gdf.close()
    
    def export_prot_graph(self) :
        """
        Creates a file as a gdf to export the graph and visualize it under Gephi.
        """
        
        graph = self.int_dict # we take the base graph, with just the proteins
        outfile_gdf = open("networkPROT.gdf","w")
        # GDF format
        outfile_gdf.write("nodedef>name VARCHAR\n")
        for key in graph.keys() :
            outfile_gdf.write(str(key)+"\n")
        outfile_gdf.write("edgedef>node1 VARCHAR, node2 VARCHAR\n")
        for key in graph.keys() :
            node1 = key
            for neighbour in graph[key] :
                node2 = neighbour
                to_write = node1, node2
                outfile_gdf.write(','.join(to_write)+"\n")
        outfile_gdf.close()
    
    ### Display the different informations available
            
    def display_caracteristics(self) :
        """
        Displays different caracteristics of the object Interactome.
        """
        
        print('Dictionnay of interactions:')
        print(self.int_dict)
        print('\nList of interactions:')
        print(self.int_list)
        print('\nList and number of proteins:')
        print(self.proteins)
        print(self.count_vertices())
        print('\nNumber of interactions:')
        print(self.count_edges())
#         print('\nNumber of interactions of protein A:')      Change protein name according to file used
#         print(self.get_degree('A'))
        print('\nThe protein with the most interactions is:')
        print(self.get_max_degree())
        print('\nThe average number of interactions is:')
        print(self.get_ave_degree())
        print('\nThe number of proteins having a degree 1 is:')
        print(self.count_degree(1))
        print('\nNumber of connected components in the graph: ')
        print(self.cc_count)
        print('\nConnected Components\' details: ')
        print(self.cc_dict)
        print('\nConnected Components\' size: ')
        print(self.cc_size)
        print('\nProtein domains in the interactome: ')
        print(self.ls_domains())
        print('\nNumber of protein domains present 10 times in the interactome: ')
        print(self.ls_domains_n(10))
        print('\nNumber of proteins having specific numbers of domains: ')
        print(self.nbDomainsByProteinDistribution(0))   # change 0 to 1 to display histogram
        print('\nNumber of domains in proteins: ')
        print(self.nbProteinsByDomainDistribution(0))   # change 0 to 1 to display histogram
        print('\nNumber of co_occurrences of domains Sema and TIG: ')
        print(self.co_occurrence('Sema', 'TIG'))


#----------------------------------------------------------------PART 6.3 PROBABILITIES---------------------------------------------------------------------------------

#




##### Testing the script #####
    
# Build the interactome
instanceInteractome = Interactome(path)
print(type(instanceInteractome.clustering('ATX1_HUMAN')))
## Make the dictionnary with domains taken from Uniprot (rebuild one or use the asved one)
# instanceInteractome.xlink_Domains()
#     # OR
# with open("dict_graph.pkl", "rb") as pklFile:
#     instanceInteractome.dict_graph = pickle.load(pklFile)

### Interactome's informations
instanceInteractome.display_caracteristics()

#instanceInteractome.export_prot_graph()
# Import in Gephi -> Yifan Hu proportional layout

# instanceInteractome.export_dom_graph(path)
# Import in Gephi -> Yifan Hu layout

# instanceInteractome.graph_threshold(path, 10)
# Import in Gephi -> Fruchterman Reingold layout