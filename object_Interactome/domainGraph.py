
import pickle
from collections import Counter
import copy
import matplotlib.pyplot as plt

path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/protein_uniprotID_name.txt'

class DomainGraph :
    """Improves an Interactome object to add the protein domains."""
    
    dict_graph = {}
    proteins = []
    domains = []
    dict_graph_dom = {}
    edges = []
    
    
    def __init__ (self, file) :
        self.dict_graph = self.open_dict_graph()
        self.proteins = self.prot_list()
        self.domains = self.ls_domains()
        self.dict_graph_dom = self.make_graph()
        self.edges = self.count_edges()
        self.nb_vertices = len(self.domains)
    
    def open_dict_graph(self) :
        """
        Uses the file created in class Interactome to retrieve the saved dictionnary.
        
        Returns
        -------
        dict
            the dictionnary saved in class Interactome corresponding to the interaction graph of proteins
        """
        
        with open("dict_graph.pkl", "rb") as pklFile:
            return(pickle.load(pklFile))
    
    def prot_list(self) :
        """
        Gives the list of proteins present in the graph.
        
        Returns
        -------
        list
            proteins of the graph
        """
        
        vertices_list = []
        for key in self.dict_graph.keys() :
            if key not in vertices_list :
                vertices_list.append(key)
        return(vertices_list)
        
    def ls_domains(self) :
        """
        Returns the list of domains present in the interaction graph.
        
        Returns
        -------
        list
            protein domains in the graph
        """
        
        ls_domains = []
        for prot in self.proteins :
            for dom in self.dict_graph[prot]['Domains'] :
                if dom not in ls_domains :
                    ls_domains.append(dom)
        return(ls_domains)
    
    def co_occurrence(self, dom_x, dom_y) :
        """
        Counts the number of times that the same two given domains are found together in the proteins.
        
        Parameters
        ----------
        dom_x : str
            a protein domain
        dom_y : str
            a protein domain
        
        Returns
        -------
        int
            the number of times dom_x and dom_y are co-occurrent in one protein
        """
        
        co_occ = 0
        for prot in self.proteins :
            if dom_x in self.dict_graph[prot]["Domains"] and dom_y in self.dict_graph[prot]["Domains"] :
                co_occ += 1
        return(co_occ)
    
    def co_occurrence_inside_protein(self, prot, dom_x, dom_y) :
        """
        Counts the number of times that two given domains are found together inside one protein.
        Includes self-co-occurrencies.
        
        Parameters
        ----------
        prot : str
            a protein name
        dom_x : str
            a protein domain
        dom_y : str
            a protein domain
        
        Returns
        -------
        int
            the number of times dom_x and dom_y are co-occurrent in one given protein
        """
        
        co_occ = 0
        values = self.dict_graph[prot]["Domains"]
        if dom_x in values and dom_y in values :
            nb_dom_x = values.count(dom_x)
            nb_dom_y = values.count(dom_y)
            if dom_x != dom_y :
                if nb_dom_x <= nb_dom_y :
                    co_occ = nb_dom_y
                if nb_dom_x > nb_dom_y :
                    co_occ = nb_dom_x
            else :
                co_occ = nb_dom_x
        return(co_occ)
    
    def make_graph(self) :
        """
        Makes an interaction graph stored in a dictionnary depicting links between domains.
        
        Returns
        -------
        dict
            the interaction garph of protein domains
        """
        
        graph = {}
                
#         # Method 1: Using the co-occurrence method, lasts at least one hour with the file HumanHighQuality (didn't let it go till the end).            
#         for dom1 in self.domains :
#             for dom2 in self.domains :
#                 if dom1 != dom2 :
#                     if self.co_occurrence(dom1, dom2) > 0 :
#                         if dom2 not in graph[dom1] :
#                             graph[dom1].append(dom2)
#                         if dom1 not in graph[dom2] :
#                             graph[dom2].append(dom1)
        
        # Method 2: Very fast compared to method 1.
        for dom in self.domains :
            graph[dom] = []
        for prot in self.proteins :
            dom_values = self.dict_graph[prot]["Domains"]
            for dom in dom_values :
                if dom not in graph.keys() :
                    graph[dom] = []
                for dom2 in dom_values :
                    if dom != dom2 :
                        if dom2 not in graph[dom] :
                            graph[dom].append(dom2)
                            
#         with open("dict_graph_dom.pkl", "wb") as pklFile: # added when working on method 1 because it is very long, but not necessary with method 2.
#             pickle.dump(graph, pklFile)
        return(graph)

    def count_edges(self) :
        """
        Computes the number of edges.
        
        Returns
        -------
        int
            the number of edges of the graph of domains
        """
        
        ls_tuple = []
        for key in self.dict_graph_dom.keys() :
            for value in self.dict_graph_dom[key] :
                uplet = (key, value)
                ls_tuple.append(uplet)
        for tuple1 in ls_tuple :
            for tuple2 in ls_tuple :
                if tuple1[0] == tuple2[1] and tuple1[1] == tuple2[0] :
                    ls_tuple.remove(tuple2)
        nb_edges = len(ls_tuple)
        return(nb_edges)
    
    def count_edges_in_graph(self, graph) :
        """
        Computes the number of edges.
        
        Parameters
        ----------
        graph : dict
            the interaction graph of proteins and domains
        
        Returns
        -------
        int
            the number of edges of the graph
        """
        
        ls_tuple = []
        for key in graph.keys() :
            for value in graph[key] :
                uplet = (key, value)
                ls_tuple.append(uplet)
        for tuple1 in ls_tuple :
            for tuple2 in ls_tuple :
                if tuple1[0] == tuple2[1] and tuple1[1] == tuple2[0] :
                    ls_tuple.remove(tuple2)
        nb_edges = len(ls_tuple)
        return(nb_edges)
    
    def density(self) :
        """
        Computes the density of the graph.
        
        Returns
        -------
        float
            the density of the graph
        """
        
        density_numerator = 2*int(self.edges)
        density_denominator = int(self.nb_vertices)*(int(self.nb_vertices)-1)
        density = density_numerator/density_denominator
        return(density)
        
    def nb_most_neighbours(self) :
        """
        Displays the 10 domains having the most neighbours.
        
        Returns
        -------
        list
            the names of the 10 domains having the highest numbers of neighbours
        """
        
        dict_neighbours = {}
        list_n = []
        for dom in self.dict_graph_dom.keys() :
            if dom not in dict_neighbours.keys() :
                dict_neighbours[dom] = []
            dict_neighbours[dom] = len(self.dict_graph_dom[dom])
        
        k = Counter(self.dict_graph_dom) 
        high = k.most_common(10) # Finding 10 highest values
        for i in high :
            list_n.append(i[0]) # We had only the domain to the list (i[1] = number of neighbours)
  
        return(list_n)

    def nb_less_neighbours(self) :
        """
        Displays the 10 domains having the less neighbours.
        
        Returns
        -------
        list
            the names of the 10 domains having the lowest numbers of neighbours
        """
        
        dict_neighbours = {}
        list_n = []
        for dom in self.dict_graph_dom.keys() :
            if dom not in dict_neighbours.keys() :
                dict_neighbours[dom] = []
            dict_neighbours[dom] = len(self.dict_graph_dom[dom])
        i = 1
        while i <= 10 :
            res = min(dict_neighbours, key=dict_neighbours.get)
            list_n.append(res)
            del dict_neighbours[res]
            i += 1
  
        return(list_n)

    def check_if_most_neighbours_in_most_frequent_domains(self) :
        """
        Compares two dictionnaries of interaction to check if the domains having the most neighbours are the most frequent in proteins.
        
        Returns
        -------
        boolean
            tells if the if the most frequent domain in the graph is also the one with the most neighbours
        """
        
        most_nei = self.nb_most_neighbours()
        
        # Step 1: compute domains frequencies
        freq_dom = {}
        for dom in self.domains :
            if dom not in freq_dom :
                freq_dom[dom] = 0
        for prot in self.dict_graph.keys() :
            for value in self.dict_graph[prot]["Domains"] :
                for sub_value in value :
                    freq_dom[value] += 1
                    
        # Step 2: Extract the most frequent domains
        freq_dom_10 = {}
        list_n = []
        k = Counter(freq_dom) 
        high = k.most_common(10) # Finding 10 highest values
        for i in high :
            list_n.append(i[0]) # We had only the domain to the list (i[1] = number of neighbours)
  
        # Step 3: Compare the most frequent domains with those with the most neighbours
        print(most_nei)
        print(list_n)
        if len(most_nei) == len(list_n) :
            for i in set(most_nei) :
                for j in set(list_n) :
                    if i == j :
                        return('TRUE')
                    else :
                        return('FALSE')

    def self_cooccurrence(self) :
        """
        Uses the function co_occurrence(dom_x, dom_y) to compute how many domains are co-occurent with themselves.
        
        Returns
        -------
        int
            the number of domains interacting with themselves
        list
            the domains that are co-occurrent with themselves
        """
        
        auto_co = []
        for dom in self.domains :
            nb = self.co_occurrence(dom, dom)
            if nb > 0 :
                if dom not in auto_co :
                    auto_co.append(dom)
        nb = len(auto_co)
        return(nb, auto_co)

    def generate_cooccurrence_graph_np(self, path, n) :
        """
        Creates a new instance of DomainGraph but alters the interaction graph to only include domains being n times co-occurrent.
        
        Parameters
        ----------
        path : str
            the path of the file for the interaction graph
        n : int
            minimal number of co-occurences that will be kept in the new graph
        
        Returns
        -------
        DomainGraph
            an object storing a graph of interactions between domains            
        """
        
        instance = DomainGraph(path)
        dict_g = copy.deepcopy(self.dict_graph_dom)
        for key in dict_g.keys() :
            for values in dict_g[key] :
                if self.co_occurrence(key, values) < n :
                    dict_g[key].remove(values)
        keys_to_del = []
        for key in dict_g.keys() :
            if len(dict_g[key]) == 0 :
                keys_to_del.append(key)
        for key in keys_to_del :
             del dict_g[key]
        instance.dict_graph_dom = dict_g
        list_dom = []
        for dom in dict_g.keys() :
            list_dom.append(dom)
        instance.domains = list_dom
        instance.edges = instance.count_edges_in_graph(dict_g)
        instance.nb_vertices = len(instance.domains)
        return(instance)

    def graph_density_cooccurrence(self) :
        """
        Creates a histogram depicting the number of domains according to the number of their co-occurencies.
        """
        
        abci = []
        ordo = []
        for n in range(0,11) :
            abci.append(n)
            instance = self.generate_cooccurrence_graph_np(path, n)
            den = instance.density()
            ordo.append(den)
        plt.plot(abci, ordo)
        plt.show()

    def generate_cooccurrence_graph_n(self, path, n) :
        """
        Creates a new DomainGraph object from the base one but domains are vertices only if they are co-occurrent at least n times in the same protein.
        Includes self-co-occurrencies.
        
        Parameters
        ----------
        path : str
            the path of the file for the interaction graph
        n : int
            minimal number of co-occurences that will be kept in the new graph
            
        Returns
        -------
        DomainGraph
            an object storing a graph of interactions between domains
        """
        instance = DomainGraph(path)
        dict_g = copy.deepcopy(self.dict_graph_dom)
        keep = []
        for prot in self.dict_graph.keys() :
            for key in self.dict_graph_dom.keys() :
                for values in self.dict_graph_dom[key] :
                    if self.co_occurrence_inside_protein(prot, key, values) >= n :
                        keep.append(key)
        for key in self.dict_graph_dom.keys() :
            for dom in self.dict_graph_dom[key] :
                if dom not in keep :
                    if dom in dict_g[key] :
                        dict_g[key].remove(dom)
        keys_to_del = []
        for key in dict_g.keys() :
            if len(dict_g[key]) == 0 :
                keys_to_del.append(key)
        for key in keys_to_del :
             del dict_g[key]
        instance.dict_graph_dom = dict_g
        list_dom = []
        for dom in dict_g.keys() :
            list_dom.append(dom)
        instance.domains = list_dom
        instance.edges = instance.count_edges_in_graph(dict_g)
        instance.nb_vertices = len(instance.domains)
        return(instance)

    def generate_cooccurrence_weighted_graph(self, path) :
        """
        Creates a new DomainGraph object from the base one but domains are vertices only if they are co-occurrent at least 1 time in the graph.
        Includes self-co-occurrencies.
        
        Parameters
        ----------
        path : str
            the path of the file for the interaction graph
            
        Returns
        -------
        DomainGraph
            an object storing a graph of interactions between domains
        """
        
        # Step 1: create an instance with domains that are co-occurrent at least one time
        instance = self.generate_cooccurrence_graph_n(path, 1)
        # we have a new dictionnary with {domA: [dom1, dom2], domB: [dom3, dom4]}, we want {domA: [(dom1, co-occ), (dom2, co-occ)], domB: [(dom3, co-occ), (dom4, co-occ)]}
        
        # Step 2: make a dictionnary of co-occurrencies between the keys and their values
        dict_complete = {}
        for dom1 in instance.dict_graph_dom.keys() :
            if dom1 not in dict_complete.keys() :
                dict_complete[dom1] = []
            for dom2 in instance.dict_graph_dom[dom1] :
                co_occ = self.co_occurrence(dom1, dom2)
                value = (dom2, co_occ)
                dict_complete[dom1].append(value)
        
        # Step 3: finalize the object
        instance.dict_graph_dom = dict_complete
        list_dom = []
        for dom in dict_complete.keys() :
            list_dom.append(dom)
        instance.domains = list_dom
        instance.edges = instance.count_edges_in_graph(dict_complete)
        instance.nb_vertices = len(instance.domains)
        return(instance)

    def generate_cooccurrence_weighted_graph_n(self, path, n) :
        """
        Creates a new DomainGraph object from the base one but domains are vertices only if they are co-occurrent at least n time in the graph.
        Includes self-co-occurrencies and multiple occurrencies inside the same protein.
        
        Parameters
        ----------
        path : str
            the path of the file for the interaction graph
        n : int
            minimal number of co-occurences that will be kept in the new graph
            
        Returns
        -------
        DomainGraph
            an object storing a graph of interactions between domains
        """
        
        # Step 1: create an instance with domains that are co-occurrent at least one time
        instance = self.generate_cooccurrence_graph_n(path, 1)
        # we have a new dictionnary {domA: [dom1, dom2], domB: [dom3, dom4]}, we want {domA: [(dom1, co-occ), (dom2, co-occ)], domB: (dom3, co-occ)}
        
        # Step 2: make a dictionnary of co-occurrencies between the keys and their values
        dict_complete = {}
        for dom1 in instance.dict_graph_dom.keys() :
            if dom1 not in dict_complete.keys() :
                dict_complete[dom1] = []
            for dom2 in instance.dict_graph_dom[dom1] :
                co_occ = self.co_occurrence(dom1, dom2)
                value = (dom2, co_occ)
                dict_complete[dom1].append(value)
        
        # Step 3: delete entries with a weight < n
        dict_corrected = copy.deepcopy(dict_complete)
        for key in dict_complete.keys() :
            for value in dict_complete[key] :
                if type(value[0]) == list : # if several domains are co-occurrent with the key
                    for dom in value : # for each domain we look at the weight
                        if dom[1] < n :
                            dict_corrected.remove(value)
                else : # if there is only one domain as value for the key
                    if value[1] < n :
                        dict_corrected[key].remove(value)
            if len(dict_corrected[key]) == 0 :
                del dict_corrected[key]
        
        # Step 4: finalize the object
        instance.dict_graph_dom = dict_corrected
        list_dom = []
        for dom in dict_corrected.keys() :
            list_dom.append(dom)
        instance.domains = list_dom
        instance.edges = instance.count_edges_in_graph(dict_corrected)
        instance.nb_vertices = len(instance.domains)
        return(instance)
    
    def display_caracteristics(self) :
        """
        Displays different caracteristics of the object Interactome.
        """
        print('The graph of domains is: ')
        print(newInstance.dict_graph_dom)
        # 6.2.8 Density
        print('\nThe density of this graph is: ')
        print(newInstance.density())
        # 6.2.9 Display the 10 domains having the most neighbours
        print('\nThe 10 domains having the most neighbours are: ')
        print(newInstance.nb_most_neighbours())
        # 6.2.10 Display the 10 domains having the less neighbours
        print('\nThe 10 domains having the less neighbours are: ')
        print(newInstance.nb_less_neighbours())
        # 6.2.11 Are the most linked domains the most frequent ones in proteins?
        print('\nAre the lists of domains having the most neighbours and the one of most frequent domains identical?')
        print(newInstance.check_if_most_neighbours_in_most_frequent_domains())
        # 6.2.12 Auto-co-occurrence
        print('\nHow many domains are co-occurent in a protein?')
        print(newInstance.self_cooccurrence())
        # 6.2.13 Graph 2
        print('\nInteraction graph of domains being at least 3 times co-occurrent: ')
        print(newInstance.generate_cooccurrence_graph_np(path, 10).dict_graph_dom)
        # 6.2.14 Density according to number of co-occurrence
        newInstance.graph_density_cooccurrence()
        print("\nThe more an interaction graph has co-occurrency among its vertices, the higher its density is.")
        # 6.2.15 
        print("\nInteraction graph of domains being at least 10 times co-occurrent inside proteins: ")
        print(newInstance.generate_cooccurrence_graph_n(path, 10).dict_graph_dom) # with the current file, there is a result for n = 1 max.
        # 6.2.16 Weighted Graph
        print("\nWeighted interaction graph of co-occurrent domains: ")
        print(newInstance.generate_cooccurrence_weighted_graph(path).dict_graph_dom)

######################################################################################################################################

newInstance = DomainGraph(path)
newInstance.display_caracteristics()

