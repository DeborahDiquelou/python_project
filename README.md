# python_project

This is the git project for the lesson of Python in M2 Bioinformatics.

Below is the list of the project's steps and its progress:

- [x] Rediscover git
- [x] Discover the Markdown language to beautify the project's readme
- [x] Read Docstring Convention (https://www.python.org/dev/peps/pep-0257/)
- [x] Chapter 1: structure, comments and deposit of the project  
- [x] Chapter 2: graph exploration, using Adama Ndar's script for chapter 1
- [x] Chapter 3: interactome object
- [x] Chapter 4: connected components
- [x] Chapter 5: protein domains
- [ ] Chapter 6: protein domains analysis
	- [x] OPT: use pickle to save the interactome
	- [x] 6.2.1: ls_proteins()
	- [x] 6.2.2: ls_domains()
	- [x] 6.2.3: ls_domains_n(n)
	- [x] OPT 6.2.4: nbDomainsByProteinDIstribution() + histogram
	- [x] OPT 6.2.5: nbProteinsByDomainDistribution() + histogram
	- [x] 6.2.6: co_occurrence(dom_x, dom_y)
	- [x] 6.2.7: class DomainGraph + generate_cooccurrence_graph
	- [x] 6.2.8: topology 1
	- [x] 6.2.9: topology 2
	- [x] 6.2.10: topology 3
	- [x] 6.2.11: topology 4
	- [x] 6.2.12: topology 5
	- [x] 6.2.13: generate_cooccurrence_graph_np(n)
	- [x] 6.2.14: graph of 6.2.13
	- [x] 6.2.15: generate_cooccurrence_graph_n()
	- [x] 6.2.16: weighted graph in DomainGraph class + weighted_cooccurrence_graph() in Interactome
	- [x] 6.2.17: export weighted graph -> cytoscape/gephi
	- [x] 6.2.18: graph_threshold(k) -> cytoscape with k = 10
	- [ ] 6.3.1: probability
	- [ ] 6.3.2: statistic test
	- [ ] 6.3.3: hardcore probability
	- [x] Docstring
- [ ] Presentation of the project
