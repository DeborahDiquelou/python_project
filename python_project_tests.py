##### Test file

import python_project as pp
from object_Interactome import object_interactome as oi
from object_Interactome import proteome as pr
import numpy
import unittest
import filecmp

### Unitary tests

test_file_basic = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/test_file.txt'
test_file_after_writeCC = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/test_file_after_writeCC.txt'
test_file_dict = {'prot_1': ['prot_2'], 'prot_2': ['prot_1', 'prot_3'], 'prot_3': ['prot_2']}
test_file_list = [('prot_1', 'prot_2'), ('prot_2', 'prot_3')]

toy_example = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/toy_example.txt'
toy_example_after_writeCC = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/toy_example_after_writeCC.txt'



class TestMethods(unittest.TestCase) :

    def test_dict_result(self) :
        """This is a test function which will check if the function read_interaction_dict is working correctly."""
        self.assertEqual(pp.read_interaction_file_dict(test_file_basic), test_file_dict)

    def test_list_result(self) :
        """This is a test function which will check if the function read_interaction_list is working correctly."""
        self.assertEqual(pp.read_interaction_file_list(test_file_basic), test_file_list)
    
#     def test_mat_result(self) :
#         """This is a test function which will check if the function read_interaction_mat is working correctly"""
#         self.assertEqual(pp.read_interaction_file_mat(test_file_basic), test_file_mat)
    
    
    def test_dict_type(self) :
        """This is a test function which will check if the function read_interaction_dict returns the correct type of result: a dictionnary."""
        test_function_dict = pp.read_interaction_file_dict(test_file_basic)
        result_type = type(test_function_dict)
        self.assertTrue(result_type == dict, msg = 'The function does not return a dictionnary like it should')
       
    def test_list_type(self) :
        """This is a test function which will check if the function read_interaction_list returns the correct type of result: a list."""
        test_function_list = pp.read_interaction_file_list(test_file_basic)
        result_type = type(test_function_list)
        self.assertTrue(result_type == list, msg = 'The function does not return a list like it should')
        
    def test_mat_type(self) :
        """This is a test function which will check if the function read_interaction_mat returns the correct type of result: a matrix."""
        test_function_mat = pp.read_interaction_file_mat(test_file_basic)
        result_type = type(test_function_mat)
        self.assertTrue(result_type == numpy.ndarray, msg = 'The function does not return a matrix like it should')
    
    #### Tests Chapter 4
    
    def test_countCC_result(self) :
        """This is a test function which will check if the function countCC() returns the correct results,
        based on a test file with known results for the tested function.
        It indirectly tests the function findCC() too, since it is used by countCC().
        """
        
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.countCC(), ({1: ['prot_1', 'prot_2', 'prot_3']}, {1: 3}, 1))
    
    def test_writeCC_result(self) :
        """
        Test the function writeCC() that is supposed to create a file listing connected components of a graph.
        The test compares a reference file to the one produced by the function inside the test.
        """
        
        test_file_INT = oi.Interactome(test_file_basic)
        test_file_INT.writeCC()
        self.assertTrue(filecmp.cmp('/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/cc_file.txt', test_file_after_writeCC, shallow=False), 'Files are different')
        
        # With a more complete file
        test_file_2 = oi.Interactome(toy_example)
        test_file_2.writeCC()
        self.assertTrue(filecmp.cmp('/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/cc_file.txt', toy_example_after_writeCC, shallow=False), 'Files are different')
        
        
    def test_extractCC_result(self) :
        """Checks if the function extractCC() will give the right result (a list of proteins) when applied to the test files."""
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.extractCC('prot_1'), (['prot_1', 'prot_2', 'prot_3']))
        
        # With a more complete file
        test_file_2 = oi.Interactome(toy_example)
        self.assertEqual(test_file_2.extractCC('G'), (['G', 'H', 'I']))
        
        
    def test_computeCC_result(self) :
        """
        Checks if the function computeCC() will find the correct CC of the protein given in argument when applied to the test files.
        The protein in arguments is indicated by its number in the list of proteins of the graph.
        """
        
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.computeCC(1), (1))
        
        # With a more complete file
        test_file_2 = oi.Interactome(toy_example)
        self.assertEqual(test_file_2.computeCC(7), (2))
        
    def test_density_result(self) :
        """This test will assert if the function density() gives the right result when applied to the test files."""
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.density(), (0.6666666666666666))
        
        # With a more complete file
        test_file_2 = oi.Interactome(toy_example)
        self.assertEqual(test_file_2.density(), (0.2222222222222222))
        
    def test_clustering_result(self) :
        """Tests if the function clustering() gives the right result when calculating the density of a protein in the test files."""
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.clustering('prot_1'), (0))
        
        # With a more complete file
        test_file_2 = oi.Interactome(toy_example)
        self.assertEqual(test_file_2.clustering('C'), (1))

##### TESTER LES FONCTIONS DE PROTEOME #######

class ExpectedFailureTestCase(unittest.TestCase) :
    
    @unittest.expectedFailure    
    def test_computeCC_result_FAIL(self) :
        """Checks if the function computeCC() does not give always the same result by comparing it to a wrong one."""
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.computeCC(1), (0))
    
    @unittest.expectedFailure
    def test_clustering_result_FAIL(self) :
        """Tests if the function clustering() does not give always the same result by comparing it to a wrong one."""
        test_file_INT = oi.Interactome(test_file_basic)
        self.assertEqual(test_file_INT.clustering('prot_1'), (1))
    

    
if __name__ == "__main__":
 
    unittest.main()


