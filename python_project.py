##### Proteins' interactions graph #####

# chapter 2 -> line 172

import os

### Format's tests

file_format_test = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/tests_files/test_file_format.txt'

def is_interaction_file(file) :
    """ This function will checks if the file follows the format needed for analyses.
    The function needs the file's path as argument and verifies several caracteristics.
    In the end, if all caracteristics are correct, the format is said of adequate format. IF not, a message indicates which caracteristic is problematic."""
    
    dict_format_checking = {}
    list_checking = ['length', 'first line', 'number of interactions', 'separator', 'number of element per line' ]
    for key in list_checking :
        dict_format_checking[key]=[]
        
    with open(file, 'r') as file_test :
        # Check that the file is not empty
        if (os.path.getsize(file)) > 0 :
            dict_format_checking['length'] = 'OK'
    
    with open(file, 'r') as file_test :
        # Check if the first line corresponds to a number different than 0
        first_line_str = file_test.readline()
        first_line = int(first_line_str)
        if type(first_line) == int and first_line != '0' :
            dict_format_checking['first line'] = 'OK'
    
    with open(file, 'r') as file_test :
        # Check if the number of interactions corresponds to the number in the first line
        number_of_lines = len(file_test.readlines())
        if (number_of_lines - 1) == first_line :
            dict_format_checking['number of interactions'] = 'OK'
    
    with open(file, 'r') as file_test :
        # Check that the interactions' lines are in he right format
        next(file_test)
        for i in file_test :
            if '\t' in i :
                dict_format_checking['separator'] = 'OK'
            elt = i.split('\t')
            if len(elt) == 2 :
                dict_format_checking['number of element per line'] = 'OK'
    
    
    is_correct = 'Yes'
    for key in dict_format_checking :
        if dict_format_checking[key] != 'OK' :
            is_correct = 'NO'
            print('The file\'s format is not correct, check the %s.' % (key,))
    if is_correct == 'Yes' :
        print('The file is in a correct format')
    
print("------------ Format verification ------------\n")
is_interaction_file(file_format_test)
print("\n------------ End of verification ------------\n")

### 1.2.1

file = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/toy_example.txt'

def read_interaction_file_dict(file) :
    """Reads an interactions' graph of proteins and convert it into a dictionnary of interactions."""
    file_test = open(file, 'r')
    network = {}
    for i in file_test :
        if '\t' in i : # if i is not the line indicating the number of interactions
            prot = i.split('\t')
            prot_A = prot[0]
            prot_B = prot[1][:-1] # delete the newline character
            if prot_A not in network.keys() :
                network[prot_A] = []
            if prot_B not in network.keys() :
                network[prot_B] = []
            if prot_B not in network[prot_A] :
                network[prot_A].append(prot_B)
            if prot_A not in network[prot_B] :
                network[prot_B].append(prot_A)
    file_test.close()
    return(network)
                
# interactions_dict = read_interaction_file_dict(file)
# print("Interactions in a dictionnary :")
# print(interactions_dict)


### 1.2.2

def read_interaction_file_list(file) :
    """Reads an interactions' graph of proteins and convert it into a list of couples of interactions."""
    file_test = open(file, 'r')
    list_couples = []
    for i in file_test :
        if '\t' in i : # if i is not the line indicating the number of interactions
            prot = i.split('\t')
            prot_A = prot[0]
            prot_B = prot[1][:-1] # delete the newline character
            proteins = (prot_A, prot_B)
            list_couples.append(proteins)
    file_test.close()
    return(list_couples)

# print("\n")
# interactions_list = read_interaction_file_list(file)
# print("Interactions in a list :")
# print(interactions_list)

### 1.2.3

import numpy as np

def read_interaction_file_mat(file) :
    """Reads an interactions' graph and convert it into an adjacency matrix."""
    file_test = open(file, 'r')
    list_list = []
    list_prot = []
    for i in file_test :
        if '\t' in i : # if i is not the line indicating the number of interactions
            prot = i.split('\t')
            prot_A = prot[0]
            prot_B = prot[1][:-1] # delete the newline character
            if prot_A not in list_prot :
                list_prot.append(prot_A)
            if prot_B not in list_prot :
                list_prot.append(prot_B)
    couples_interactions = read_interaction_file_list(file)
    for i in list_prot :
        interactions_for_prot_i = []
        for j in list_prot :
            if (i, j) in couples_interactions or (j, i) in couples_interactions :
                interactions_for_prot_i.append('1')
            else :
                interactions_for_prot_i.append('0')
        list_list.append(interactions_for_prot_i)
    matrix = np.array(list_list)
    file_test.close()
    return(matrix)

# print("\n")
# interactions_mat = read_interaction_file_mat(file)
# print("Interactions in an adjacency matrix :")
# print(interactions_mat)
# print(type(interactions_mat))

### 1.2.4

def read_interaction_file(file) :
    """Reads an interactions( graph and returns the dictionnary and the list of interactions."""
    d_int = read_interaction_file_dict(file)
    l_int = read_interaction_file_list(file)
    couple = (d_int, l_int)
    return(couple)

# print("\n")
# dict_list = read_interaction_file(file)
# print("Dictionnary and list of interactions :")
# print(dict_list)

### 1.2.5

# Cut the interactions' graph in several graphs, perform the function read_interaction_file for each graph and merge the results.

### 1.2.6

# cf python_project_tests.py



######################### CHAPTER 2 #########################















