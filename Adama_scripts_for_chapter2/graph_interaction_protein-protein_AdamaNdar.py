#################### Adama Ndar's script ####################

#!/usr/bin/python3
# encoding: utf-8

graph_file = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/toy_example.txt'

#graph_file = "test.txt"
#graph_file2 = "toy_example.txt"

"""
read_interaction_file_dict take a single argument(graph_file2) the name of the file and reads a graph of interactions between proteins and stores it in a dictionary (dict_interaction).
read_interaction_file_dict reaturn dict_interaction.

"""
def read_interaction_file_dict(graph_file2):
    
    dict_interaction = {}
    file = open(graph_file2, "r") 
    data = file.readlines()[1:]
    
    for line in data:
        new_data = line.split()
        
        summit = new_data[0]
        summit_neighboor = new_data[1]
       
        if not (summit in dict_interaction):
            dict_interaction[summit] = set()
            dict_interaction[summit].add(summit_neighboor)
            
        if (summit in dict_interaction):
            dict_interaction[summit].add(summit_neighboor)
            
        if not (summit_neighboor in dict_interaction):
            dict_interaction[summit_neighboor] = set()
            dict_interaction[summit_neighboor].add(summit)
            
        if (summit in dict_interaction):
            dict_interaction[summit_neighboor].add(summit)
                        
    file.close()
    return(dict_interaction)
    
#print(read_interaction_file_dict(graph_file))
    
 
"""
read_interaction_file_list take a single argument(graph_file2) the name of the file and reads a graph of interactions between proteins and stores it in a couple list (list_interaction).
read_interaction_file_list reaturn list_interaction.

""" 
def read_interaction_file_list(graph_file2):

    list_interaction = []
    file = open(graph_file2, "r") 
    data = file.readlines()[1:]
    
    for line in data:
        new_data = line.split()
        
        summit = new_data[0]
        summit_neighboor = new_data[1]
        couples = (summit, summit_neighboor)
        list_interaction.append(couples)
        
    file.close()
    return(list_interaction)

#print(read_interaction_file_list(graph_file))


"""
read_interaction_file take a single argument(graph_file2) the name of the file and reads a graph of interactions between proteins and returns a couple (d_int, l_int) whose first element is the dict_interaction representing the graph, 
and the second element is the list_interaction representing the even graph.

"""
def read_interaction_file(graph_file2):
    file = open(graph_file2, "r") 
    data = file.readlines()[1:]
    
    for line in data:
        new_data = line.split()
        d_int = read_interaction_file_dict(graph_file2)
        l_int = read_interaction_file_list(graph_file2)
        
    file.close()                
    return(d_int, l_int)

#print(read_interaction_file(graph_file))

#################### End of Adama's script ####################


#################### Chapter 2 ####################

########## Part 1 ##########

### 2.1.1
def count_vertices(file) :
    """Takes a graph file as argument and returns the number of vertices in the interactions' graph."""
    file_dict = read_interaction_file_dict(file)
    number_keys = len(file_dict)
    return(number_keys)

#print(count_vertices(graph_file))

### 2.1.2
def count_edges(file) :
    """Takes a graph file as argument and returns the number of edges in the interactions' graph."""
    file_list = read_interaction_file_list(file)
    number_edges = len(file_list)
    return number_edges

#print(count_edges(graph_file))

### 2.1.3
def clean_interactome(file) :
    """Takes a graph file as argument and creates another one without interactions present in several copies in the first file, or without auto-interactions."""
    file_list = read_interaction_file_list(file)
    list_interactions = []
    for uplet in file_list :
        prot_A = uplet[0]
        prot_B = uplet[1]
        uplet_2 = (prot_B, prot_A)
        if uplet not in list_interactions :
            if uplet_2 not in list_interactions :
                if prot_A != prot_B :
                    list_interactions.append(uplet)
    number_interactions = str(len(list_interactions))
    graph_cleaned = open('graph_cleaned', 'w')
    graph_cleaned.write(str(number_interactions + '\n'))
    for uplet in list_interactions :
        prot_A = uplet[0]
        prot_B = uplet[1]
        new_interaction = prot_A, prot_B
        new_interaction_bis = '\t'.join(new_interaction)
        graph_cleaned.write(str(new_interaction_bis + '\n'))
    graph_cleaned.close()

#clean_interactome(graph_file)

########## Part 2 ##########

### 2.2.1
def get_degree(file, prot) :
    """Takes a graph file as argument, and a specified protein to count the number of interactions of this protein in the graph."""
    file_dict = read_interaction_file_dict(file)
    for protein in file_dict.keys() :
        if prot == protein :
            interactions = file_dict[protein]
            number_interactions = len(interactions)
    return(number_interactions)

#print(get_degree(graph_file, 'B'))

### 2.2.2
def vertices_list(file) :
    """Takes a graph file as argument to return the list of vertices present in the graph."""
    file_dict = read_interaction_file_dict(file)
    vertices_list = []
    for key in file_dict.keys() :
        vertices_list.append(key)
    return(vertices_list)
        

def get_max_degree(file) :
    """Takes a graph file as argument and returns the protein having the highest number of interactions in the graph."""
    degree_list = []
    clean_interactome(file)
    path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/graph_cleaned'
    vertices = vertices_list(path)
    for protein in vertices :
        degree = get_degree(path, protein)
        degree_uplet = (protein, degree)
        degree_list.append(degree_uplet)
    degree_max = 0
    for uplet in degree_list :
        protein = uplet[0]
        degree = uplet[1]
        if degree > degree_max :
            degree_max = degree
            protein_max = protein
    return(protein_max)

#print(get_max_degree(graph_file))

### 2.2.3
def get_ave_degree(file) :
    """Takes a graph file as argument and returns the average number of interactions in the graph."""
    clean_interactome(file)
    path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/graph_cleaned'
    degrees = []
    proteins_list = vertices_list(path)
    for protein in proteins_list :
        degrees.append(get_degree(path, protein))
    vertices = count_vertices(path)
    degree_sum = sum(degrees)
    ave_degree = degree_sum / vertices
    return(ave_degree)

#print(get_ave_degree(graph_file))

### 2.2.4
def count_degree(file, deg) :
    """Takes a graph file as argument and a number of interactions to return the number of proteins having this specific degree of interactions."""
    clean_interactome(file)
    path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/graph_cleaned'
    vertices = vertices_list(path)
    degree_list = []
    for protein in vertices :
        degree = get_degree(path, protein)
        prot_degree = (protein, degree)
        degree_list.append(prot_degree)
    count = 0
    for uplet in degree_list :
        protein = uplet[0]
        degree = uplet[1]
        if degree == deg :
            count += 1
    return(count)

# print(count_degree(graph_file, 4))

### 2.2.5
import math

def histogram_degree(file, dmin, dmax) :
    """
    Takes a graph file as argument, and a range of interactions' number to return for each degree inside this range, the number of proteins having this number of interactions.
    The result is displayed by a number indicating the degree followed by a number of asterisks representing the number of proteins having this degree of interactions.
    """
    
    clean_interactome(file)
    path = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/graph_cleaned'
    proteins_degrees = []
    dmax = dmax + 1
    for d in range(dmin, dmax) :
        count = count_degree(path, d)
        degree_count = (d, count)
        proteins_degrees.append(degree_count)
    for uplet in proteins_degrees :
        number_prot_degree = ''
        deg = str(uplet[0])
        count = uplet[1]
        
        # To have a scale in the representation of interactions
        count_truncate = math.trunc(count/10)
        
        for number in range(count_truncate) :
            number_prot_degree += '*'
        print(deg + number_prot_degree)
    

histogram_degree(graph_file, 1, 50)





