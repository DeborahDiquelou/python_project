#!/usr/bin/python3
# encoding: utf-8

#################### Adama Ndar's script ####################

import graph_interaction_protein-protein_AdamaNdar as gp
import unittest


graph_file2 = "toy_example.txt"

test_file = '/home/deborah/Documents/M2_Bioinformatique/UE_projet_python/python_project/test_file.txt'
test_file_vertices = '3'
test_file_edges = '2'

class tests_functions(unittest.TestCase):   

    """
     test_interaction_file_dict checks the type of function read_interaction_file_dict
    """
    def test_interaction_file_dict(self):
        dict_type = type(gp.read_interaction_file_dict(graph_file2))
        self.assertEqual(dict_type, dict)
        
        
    """
    test_interaction_file_list checks the type of function read_interaction_file_list
    """    
    def test_interaction_file_list(self):
        list_type = type(gp.read_interaction_file_list(graph_file2))
        self.assertEqual(list_type, list)
    
    """
    test_interaction checks the type of function read_interaction_file
    """
    def test_interaction(self):
        tuple_type = type(gp.read_interaction_file(graph_file2))
        self.assertEqual((tuple_type), tuple)

#################### End of Adama Ndar's script ####################

##### Chapter 2, question 2.1.4 #####
        
    def test_count_vertices(self) :
        """This is a test function which will check if the function count_vertices is working correctly."""
        self.assertEqual(gp.count_vertices(test_file), test_file_vetices)
    
    def type_count_vertices(self) :
        """ This test function checks if the type of the value returned by the function count_vertices is correct."""
        vertices_type = type(gp.count_vertices(test_file))
        self.assertTrue(vertices_type == int, msg = 'The function does not return an integer like it should')


    def test_count_edges(self) :
        """This is a test function which will check if the function count_edges is working correctly."""
        self.assertEqual(gp.count_edges(test_file), test_file_edges)
    
    def type_count_edges(self) :
        """ This test function checks if the type of the value returned by the function count_edges is correct."""
        edges_type = type(gp.count_edges(test_file))
        self.assertEqual(edges_type == int, msg = 'The function does not return an integer like it should')

if __name__ == '__main__':
    unittest.main()
